from unittest import TestCase

from trees import Node


class NodeTestCase(TestCase):
    def test_equality(self):
        self.assertEqual(Node(1), Node(1))
        self.assertNotEqual(Node(1), Node("foo"))

        with self.assertRaises(NotImplementedError):
            Node(1) == ["foo"]
