from unittest import TestCase

from trees import Node, Tree


class TreeTestCase(TestCase):
    def setUp(self):
        self.tree = Tree(
            Node(1, [Node(2, [Node(3), Node(4)]), Node(5, [Node(6)]), Node(7)])
        )

    def test_contains(self):
        self.assertTrue(self.tree.contains(1))
        self.assertTrue(self.tree.contains(6))
        self.assertFalse(self.tree.contains(-1))

    def test_leaves(self):
        self.assertSequenceEqual(
            self.tree.leaves(), [Node(3), Node(4), Node(6), Node(7)]
        )
