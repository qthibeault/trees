from __future__ import annotations

from typing import Generic, List, Optional, TypeVar, Sequence

_T = TypeVar("_T")


class Node(Generic[_T]):
    def __init__(self, value: _T, children: Optional[Sequence[Node[_T]]] = None):
        self.value = value
        self.children = list(children) if children is not None else []

    def __add__(self, other: Node[_T]) -> Node[_T]:
        return Node(self.value, self.children + [other])

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Node):
            raise NotImplementedError()

        return self.value == other.value
