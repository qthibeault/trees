from __future__ import annotations

from typing import Generic, List, TypeVar

from .node import Node

_T = TypeVar("_T")


def _contains(node: Node[_T], value: _T) -> bool:
    if node.value == value:
        return True

    return any(_contains(child, value) for child in node.children)


def _leaves(node: Node[_T]) -> List[Node[_T]]:
    if len(node.children) == 0:
        return [node]

    return [leaf for child in node.children for leaf in _leaves(child)]


class Tree(Generic[_T]):
    def __init__(self, root: Node[_T]):
        self.root = root

    def contains(self, value: _T) -> bool:
        return _contains(self.root, value)

    def leaves(self) -> List[Node[_T]]:
        return _leaves(self.root)


class BinaryTree(Tree[_T]):
    def __init__(self, root: Node[_T]):
        if len(root.children) > 2:
            raise ValueError("Binary trees only support 2 children")

        self.root = root

    @property
    def left(self) -> BinaryTree[_T]:
        return BinaryTree(self.root.children[0])

    @property
    def right(self) -> BinaryTree[_T]:
        return BinaryTree(self.root.children[1])
