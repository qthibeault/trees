Trees
=====
A small, typed library for representing trees in python.

Usage
-----
```python
from trees import Node, Tree

leaves = [Node(1), Node(2), Node(3)]
root = Node(4, leaves)
tree = Tree(root)
```

Some simple tree operations are supported:
```python
has_three = tree.contains(3)
tree_leaves = tree.leaves()
```

Installation
------------
Trees is available for installation using the command `pip install trees`

